import matplotlib.pyplot as plt
import numpy as np

t = np.linspace(0,8,2000)
y1 = -(np.exp(-2*t))*np.cos(np.sqrt(5)*t) - (11/np.sqrt(5))*(np.exp(-2*t))*np.sin(np.sqrt(5)*t) + 1

y2 = -(np.exp(-(0.7)*t))*np.cos((2.917)*t) - (3.325)*(np.exp(-(0.7)*t))*np.sin((2.917)*t) + 1

ref = 1 + 0*t

plt.grid(linestyle = '--')
plt.plot(t,y1,label='$H(s)$',color='red')
plt.plot(t,y2,label='$G(s)$',color='blue')
plt.plot(t,ref,label='$u(t)$',color='black')
plt.xlabel('$t$')
plt.ylabel('$y(t)$')
plt.legend()
plt.xlim(0,8)
plt.savefig('../ejercicio2.png', format='png')

